package com.jsn.buildbase

import android.annotation.TargetApi
import android.content.res.Resources
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.marginBottom
import androidx.core.view.updatePadding
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.*
import com.jsn.baselibx.ui.BaseActivity
import com.jsn.buildbase.algorithm.multiply
import com.jsn.buildbase.algorithm.reverseWords
import com.jsn.buildbase.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity :BaseActivity<ActivityMainBinding>(){
    var mainViewModel: MainViewModel? = null
    override fun getContentView(): Int {
        return R.layout.activity_main
    }

    private lateinit var appBarConfiguration : AppBarConfiguration

    override fun initUI() {
        //multiply("2","3")
        reverseWords("a good   example")

        setSupportActionBar(mBinding.toolbar)

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host) as NavHostFragment? ?: return

        // Set up Action Bar
        val navController = host.navController

        appBarConfiguration= AppBarConfiguration(setOf(R.id.weather_fragment,R.id.todo_fragment))

        setupActionBarWithNavController(navController,appBarConfiguration)

        //setbottom naviagtion
        val bottomNav=mBinding.bottomNavView.apply {
            setupWithNavController(navController)
        }



        navController.addOnDestinationChangedListener{_,destination, _ ->
            val dest: String = try {
                resources.getResourceName(destination.id)
            } catch (e: Resources.NotFoundException) {
                Integer.toString(destination.id)
            }

            title=dest
            Log.d("NavigationActivity", "Navigated to $dest")
        }

    }

    @RequiresApi(Build.VERSION_CODES.KITKAT_WATCH)
    fun applyInsets(){
        bottom_nav_view.setOnApplyWindowInsetsListener{ view,insets->
            //view.updatePadding(bottom = insets.systemWindowInsetBottom)
            //view.marginBottom=insets.systemWindowInsetBottom
            insets
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT_WATCH)
    override fun initData() {
        root.systemUiVisibility= View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // TODO  - Have Navigation UI Handle the item selection - make sure to delete
        //  the old return statement above
//        // Have the NavigationUI look for an action or destination matching the menu
//        // item id and navigate there if found.
//        // Otherwise, bubble up to the parent.
        return item.onNavDestinationSelected(findNavController(R.id.nav_host)) //
                || super.onOptionsItemSelected(item)

    }

    // TODO - Have NavigationUI handle up behavior in the ActionBar
    override fun onSupportNavigateUp(): Boolean {
        // Allows NavigationUI to support proper up navigation or the drawer layout
        // drawer menu, depending on the situation
        return findNavController(R.id.nav_host).navigateUp(appBarConfiguration)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val retValue= super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.bottom_nav_menu,menu)
        return retValue
    }

}
