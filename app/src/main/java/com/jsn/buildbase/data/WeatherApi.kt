package com.jsn.buildbase.data

import com.jsn.buildbase.data.dataclass.RealtimeWeather
import retrofit2.http.GET

const val apiToken ="HcWdZ5fkEp2qajoD"

interface WeatherApi {
    /* https://api.caiyunapp.com/v2/HcWdZ5fkEp2qajoD/121.6544,25.1552/realtime.json*/
    /*realtime weather hangzhou zhejiang china*/
    @GET(apiToken+"/120.12,30.16/realtime.json")
    suspend fun getHangzhouWeatherNow() : RealtimeWeather

}