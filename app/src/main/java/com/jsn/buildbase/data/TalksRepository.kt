package com.jsn.buildbase.data

import com.jsn.baselibx.retrofit.RetrofitApiFactory

class TalksRepository private constructor(){

    val service=RetrofitApiFactory.createApi(TedApi::class.java)

    suspend fun talksByTextRemote(text:String, size:String?) :String{
        return service.getTalk(text,size)
    }

 companion object{

     @Volatile private var instance :TalksRepository? = null

     fun getInstance():TalksRepository=
         instance?: synchronized(this){
             instance?: TalksRepository().also { instance=it }
         }
 }
}