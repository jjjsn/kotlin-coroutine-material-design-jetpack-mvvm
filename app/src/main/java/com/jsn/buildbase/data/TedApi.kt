package com.jsn.buildbase.data

import retrofit2.http.GET
import retrofit2.http.Query

interface TedApi {


    /*OkHttpClient client = new OkHttpClient();
Request request = new Request.Builder()
	.url("https://bestapi-ted-v1.p.rapidapi.com/transcriptFreeText?text=amazing%20talk")
	.get()
	.addHeader("x-rapidapi-host", "bestapi-ted-v1.p.rapidapi.com")
	.addHeader("x-rapidapi-key", "e99e91bc2fmsh6a1f6e1d03ab5b4p1bdd7bjsn2e3295f8b956")
	.build();

Response response = client.newCall(request).execute();*/

    @GET("transcriptFreeText")
    suspend fun getTalk(@Query("text") text:String,@Query("size") size:String?=null) : String
}