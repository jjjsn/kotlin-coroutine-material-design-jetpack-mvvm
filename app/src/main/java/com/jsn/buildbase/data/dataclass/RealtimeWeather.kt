package com.jsn.buildbase.data.dataclass

data class RealtimeWeather(
    var api_status: String,
    var api_version: String,
    var lang: String,
    var location: List<String>,
    var result: Result,
    var server_time: String,
    var status: String,
    var tzshift: String,
    var unit: String
) {
    data class Result(
        var apparent_temperature: Double, //tempture
        var aqi: String, //api version
        var cloudrate: String, //
        var co: String,
        var comfort: Comfort,
        var dswrf: String,
        var humidity: String,
        var no2: String,
        var o3: String,
        var pm10: String,
        var pm25: String,
        var precipitation: Precipitation,
        var pres: String,
        var skycon: String,
        var so2: String,
        var status: String,
        var temperature: String,
        var ultraviolet: Ultraviolet,
        var visibility: String,
        var wind: Wind
    ) {
        data class Comfort(
            var desc: String,
            var index: String
        )

        data class Precipitation(
            var local: Local,
            var nearest: Nearest
        ) {
            data class Local(
                var datasource: String,
                var intensity: String,
                var status: String
            )

            data class Nearest(
                var distance: String,
                var intensity: String,
                var status: String
            )
        }

        data class Ultraviolet(
            var desc: String,
            var index: String
        )

        data class Wind(
            var direction: String,
            var speed: String
        )
    }
}