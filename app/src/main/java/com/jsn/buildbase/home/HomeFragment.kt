package com.jsn.buildbase.home

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.view.View
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat
import androidx.core.view.marginBottom
import androidx.core.view.updateLayoutParams
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsn.baselibx.ui.BaseFragment
import com.jsn.baselibx.util.showMessageShort
import com.jsn.baselibx.utils.Logs
import com.jsn.baselibx.widget.BaseRVAdapter
import com.jsn.baselibx.widget.BaseViewHolder
import com.jsn.buildbase.R
import com.jsn.buildbase.audio.AudioActivity
import com.jsn.buildbase.databinding.FragmentHomehahaBinding

import com.jsn.buildbase.utilities.FLAGS_FULL
import com.jsn.buildbase.utilities.InjectorUtil
import com.jsn.buildbase.utilities.doOnApplyWindowInsets
import kotlinx.android.synthetic.main.fragment_homehaha.*
import kotlin.math.roundToInt

internal val Int.dp: Int get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()  //do to px

internal val Int.px: Int get() = (this / Resources.getSystem().displayMetrics.density).roundToInt()


class HomeFragment : BaseFragment<FragmentHomehahaBinding>() {

    private val viewModel:HomeViewModel by viewModels {
        InjectorUtil.provideHomeViewModelFactory()
    }

    override fun getContentView(): Int {
        return R.layout.fragment_homehaha
    }
    override fun initUI() {
        mBinding.viewModel=viewModel
        subscribeUI()
        bt_music.setOnClickListener{
            AudioActivity.show(requireActivity())
        }

    }


    @RequiresApi(Build.VERSION_CODES.KITKAT_WATCH)
    override fun initData() {
        ViewCompat.setNestedScrollingEnabled(mBinding.rv,false)
        mBinding.rv.layoutManager=LinearLayoutManager(requireContext())
        mBinding.rv.adapter=Adapter(requireContext()).also { it.setData(data) }

        Logs.e("fuckinitMargin :+${fab.marginBottom.px}")


        fab.doOnApplyWindowInsets{v,inset,padding,originalMargin-> //padding :original pading
            //so we pad the fab
            //v.updatePaddingRelative(bottom = padding.bottom + inset.systemWindowInsetBottom)
            v.updateLayoutParams<ConstraintLayout.LayoutParams> {
                bottomMargin = originalMargin + inset.systemWindowInsetBottom
                Logs.e("fuckinset: +${inset.systemWindowInsetBottom.px}")
            }
            Logs.e("fuckapplyMargin: +${fab.marginBottom.px}")
        }

        srl.setOnRefreshListener {
          /* findNavController().navigate(R.id.action_to_home2,null)*/
            srl.isRefreshing=false
        }

        rv.systemUiVisibility= FLAGS_FULL
    }

    private fun subscribeUI() {
        //live data extension
        viewModel.msg.observe(viewLifecycleOwner) { msg ->
            requireActivity().showMessageShort(msg)
        }
        viewModel.buttonEnabled.observe(viewLifecycleOwner){enabled ->
            mBinding.bt.isEnabled=enabled
        }
    }

    val data by lazy{
        ArrayList<Any>().apply {
            for(i in 1..50)
            TextView(requireContext()).also {
                it.text="11111111"
                add(it)
            }
        }
    }

    inner class Adapter(context: Context): BaseRVAdapter<Any, BaseViewHolder>(context){
        override fun holderInstance(binding: ViewDataBinding): BaseViewHolder {
           return object:BaseViewHolder(binding){
               override fun bindData(item: Any?) {
                   super.bindData(item)
               }
               override fun doClick(view: View) {
                   super.doClick(view)
               }
           }
        }

        override fun layoutResId(viewType: Int): Int {
            return R.layout.item_simple_text
        }

    }

}