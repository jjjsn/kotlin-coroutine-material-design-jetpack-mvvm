package com.jsn.buildbase.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jsn.buildbase.data.TalksRepository

@Suppress("UNCHECKED_CAST")
class HomeViewModelFactory(
    private val talksRepository: TalksRepository)
    : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(talksRepository) as T
    }
}