package com.jsn.buildbase.home

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.jsn.baselibx.ui.BaseFragment
import com.jsn.baselibx.util.showMessageShort
import com.jsn.baselibx.widget.BaseRVAdapter
import com.jsn.baselibx.widget.BaseViewHolder
import com.jsn.buildbase.R
import com.jsn.buildbase.audio.AudioActivity
import com.jsn.buildbase.databinding.FragmentHomehahaBinding

import com.jsn.buildbase.utilities.InjectorUtil
import kotlinx.android.synthetic.main.fragment_homehaha.*

class TestFragment : BaseFragment<FragmentHomehahaBinding>() {




    private val viewModel:HomeViewModel by viewModels {
        InjectorUtil.provideHomeViewModelFactory()
    }

    override fun getContentView(): Int {
        return R.layout.fragment_homehaha
    }
    override fun initUI() {
        mBinding.viewModel=viewModel
        subscribeUI()
        bt_music.setOnClickListener{
            AudioActivity.show(requireActivity())
        }
    }


    override fun initData() {
        ViewCompat.setNestedScrollingEnabled(mBinding.rv,false)
        mBinding.rv.layoutManager=LinearLayoutManager(requireContext())
        mBinding.rv.adapter=Adapter(requireContext()).also { it.setData(data) }
        srl.setOnRefreshListener {
            /*findNavController().navigate(R.id.action_to_home2,null)*/
            srl.isRefreshing=false
        }

    }

    private fun subscribeUI() {
        //live data extension
        viewModel.msg.observe(viewLifecycleOwner) { msg ->
            requireActivity().showMessageShort(msg)
        }
        viewModel.buttonEnabled.observe(viewLifecycleOwner){enabled ->
            mBinding.bt.isEnabled=enabled
        }
    }

    val data by lazy{
        ArrayList<Any>().apply {
            for(i in 1..50)
                TextView(requireContext()).also {
                    it.text="11111111"
                    add(it)
                }
        }
    }

    inner class Adapter(context: Context): BaseRVAdapter<Any, BaseViewHolder>(context){
        override fun holderInstance(binding: ViewDataBinding): BaseViewHolder {
            return object:BaseViewHolder(binding){
                override fun bindData(item: Any?) {
                    super.bindData(item)
                }
                override fun doClick(view: View) {
                    super.doClick(view)
                }
            }
        }

        override fun layoutResId(viewType: Int): Int {
            return R.layout.item_simple_text
        }

    }

}