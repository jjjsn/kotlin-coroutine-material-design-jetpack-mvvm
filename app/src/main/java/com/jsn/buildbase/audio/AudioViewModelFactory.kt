package com.jsn.buildbase.audio

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.net.URI


const val MUSIC_URL="asset:///music.mp3"
@Suppress("UNCHECKED_CAST")
class AudioViewModelFactory(private val uri: Uri?=Uri.parse(MUSIC_URL))
    : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AudioViewModel(uri) as T
    }
}