package com.jsn.buildbase

import androidx.multidex.MultiDex
import com.jsn.baselibx.MApp

class App : MApp() {
    companion object{

    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
    }

}