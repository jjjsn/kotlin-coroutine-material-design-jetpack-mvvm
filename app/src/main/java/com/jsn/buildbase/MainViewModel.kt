package com.jsn.buildbase

import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import java.util.concurrent.atomic.AtomicBoolean

class MainViewModel :ViewModel() {

    val _data = MutableLiveData<String>()

    val data:LiveData<Int> =_data.switchMap {
        MutableLiveData<Int>().apply {
            postValue(Int.MAX_VALUE)
        }
    }
}