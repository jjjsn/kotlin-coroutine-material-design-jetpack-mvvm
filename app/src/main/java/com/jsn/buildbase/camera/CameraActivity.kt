package com.jsn.buildbase.camera

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Camera
import android.view.SurfaceHolder
import android.view.SurfaceView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.jsn.baselibx.ui.BaseActivity
import com.jsn.buildbase.R
import com.jsn.buildbase.databinding.ActivityCameraBinding
import java.util.jar.Manifest


class CameraActivity:BaseActivity<ActivityCameraBinding>(){
    override fun getContentView(): Int {
       return R.layout.activity_camera
    }

    private val REQUEST_CAMERA: Int
        get() {
            return 1
        }

    lateinit var cameraView:CameraSurfaceView

    lateinit var  cameras:Array<Camera>

    override fun initUI() {
        //ASSUME that permisison is ok
        val checkSelfPermission =
            ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
        if(checkSelfPermission==PackageManager.PERMISSION_GRANTED){
            val cameraSurfaceView = CameraSurfaceView(this).apply {
                mBinding.content.addView(this)
            }.also { cameraView=it }
        }else{

            ActivityCompat.requestPermissions(
                this, arrayOf(
                    android.Manifest.permission.CAMERA
                ), REQUEST_CAMERA )

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            REQUEST_CAMERA ->{
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    val cameraSurfaceView = CameraSurfaceView(this).apply {
                        mBinding.content.addView(this)
                    }.also { cameraView=it }
                }
            }
            else->this.showMessage("permission denied").also { finish() }
        }
    }

    override fun initData() {

    }

    override fun onResume() {
        super.onResume()
        CameraUtils.startPreview()
    }

    override fun onPause() {
        super.onPause()
        CameraUtils.stopPreview()
    }

    override fun onDestroy() {
        super.onDestroy()
        CameraUtils.releaseCamera()
    }

    inner class CameraSurfaceView(context: Context):SurfaceView(context) ,SurfaceHolder.Callback{

         var surfaceHolder: SurfaceHolder

        init {
            surfaceHolder=holder
            surfaceHolder.addCallback(this)
        }

        override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
            CameraUtils.startPreviewDisplay(holder);
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {
            CameraUtils.releaseCamera();
        }

        override fun surfaceCreated(holder: SurfaceHolder?) {
            CameraUtils.openFrontalCamera(CameraUtils.DESIRED_PREVIEW_FPS)
        }

    }

}

