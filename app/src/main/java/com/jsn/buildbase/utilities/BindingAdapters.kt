package com.jsn.buildbase.utilities

import android.view.View
import android.widget.Button
import androidx.databinding.BindingAdapter

@BindingAdapter("isGone")
fun bindIsGone(view:View,isGone:Boolean){
    view.visibility=if(isGone){
        View.GONE
    } else{
        View.VISIBLE
    }
}

@BindingAdapter("isEnabled")
fun bindIsEnabled(view:View,isEnabled:Boolean){
    view.isEnabled=isEnabled
}