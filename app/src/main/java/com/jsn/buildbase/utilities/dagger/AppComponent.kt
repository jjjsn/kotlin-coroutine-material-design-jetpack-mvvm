package com.jsn.buildbase.utilities.dagger

import com.jsn.buildbase.weather.WeatherFragment
import dagger.Component
import javax.inject.Inject


// Definition of the Application graph
@Component
interface AppComponent {
    fun inject(where: WeatherFragment)
}



class Info @Inject constructor(){
    val text="info"
}