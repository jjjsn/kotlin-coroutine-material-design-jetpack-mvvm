package com.jsn.buildbase.utilities

import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.amap.api.maps.AMap
import com.jsn.buildbase.audio.AudioViewModelFactory
import com.jsn.buildbase.audio.MUSIC_URL
import com.jsn.buildbase.data.TalksRepository
import com.jsn.buildbase.weather.WeatherRepository
import com.jsn.buildbase.home.HomeViewModelFactory
import com.jsn.buildbase.weather.DetailViewModelFactory
import com.jsn.buildbase.weather.WeatherViewModelFactory
import com.jsn.buildbase.weather.ui.map.MapWeatherViewModelFactory


/*手动依赖注入*/
object InjectorUtil {

    fun provideMapWeatherViewModelFactory(aMap: AMap)=MapWeatherViewModelFactory(aMap)

    fun provideWeatherViewModelFactory() :WeatherViewModelFactory{
        val weatherRepoository = getWeatherRepoository()
        return WeatherViewModelFactory(weatherRepoository)
    }


    fun getWeatherRepoository(): WeatherRepository {
        return  WeatherRepository.getInstance()
    }


    //provide repository
    fun getTalksRepository() :TalksRepository{
        return TalksRepository.getInstance()
    }


    //provide Factory
    fun provideHomeViewModelFactory() :HomeViewModelFactory{
        val repository= getTalksRepository()
        return  HomeViewModelFactory(repository)
    }

    fun provideAudioViewModelFactory(uri: Uri?=Uri.parse(MUSIC_URL)) :AudioViewModelFactory{
        return AudioViewModelFactory(uri)
    }
    
    
    //detail vewModelfactory


    fun detailViewModelFactory (value :Int): DetailViewModelFactory =
        DetailViewModelFactory(getWeatherRepoository(),value)
}

/**
 * For Fragments, allows declarations like
 * ```
 * val myViewModel = viewModelProvider(myViewModelFactory)
 * ```
 */
inline fun <reified VM : ViewModel> Fragment.viewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProviders.of(this, provider).get(VM::class.java)
