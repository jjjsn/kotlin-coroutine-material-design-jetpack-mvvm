package com.jsn.buildbase.weather

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jsn.baselibx.ui.BaseActivity
import com.jsn.baselibx.widget.BaseRVAdapter
import com.jsn.buildbase.R
import com.jsn.buildbase.databinding.ActivityNestBinding
import com.jsn.buildbase.databinding.ItemIntBinding

class NestActivity :BaseActivity<ActivityNestBinding>() {

    lateinit var adapter: Adapter
    override fun getContentView(): Int {
        return R.layout.activity_nest
    }

    override fun initUI() {
        mBinding.rv.layoutManager=LinearLayoutManager(getContext())
        mBinding.rv.adapter=Adapter(diff).also { adapter=it }
        val list = mutableListOf<Int>().apply {
            for(i in 0..30) {
                add(i)
            }
        }
        adapter.submitList(list)

    }

    override fun initData() {

    }

    val diff=object :DiffUtil.ItemCallback<Int>(){
        override fun areItemsTheSame(oldItem: Int, newItem: Int): Boolean {
            return  oldItem==newItem
        }

        override fun areContentsTheSame(oldItem: Int, newItem: Int): Boolean {
            return  oldItem==newItem
        }

    }

    inner class Adapter(itemCallback: DiffUtil.ItemCallback<Int>):ListAdapter<Int,IntHolder>(itemCallback){
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntHolder {
            val binding: ItemIntBinding = ItemIntBinding.inflate(layoutInflater, parent, false)
            return IntHolder(binding)
        }

        override fun onBindViewHolder(holder: IntHolder, position: Int) {
            holder.bind(getItem(position))
        }


    }

    class IntHolder(private val binding: ItemIntBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item: Int){
            binding.tv.text=item.toString()
        }
    }
}

