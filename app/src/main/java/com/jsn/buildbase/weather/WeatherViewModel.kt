package com.jsn.buildbase.weather

import androidx.lifecycle.*
import com.jsn.buildbase.data.dataclass.RealtimeWeather
import com.jsn.buildbase.data.dataclass.Result
import com.jsn.buildbase.home.doData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class WeatherViewModel@Inject constructor(private val repository: WeatherRepository ) :ViewModel(){

    val _hangZhouRealtime:MutableLiveData<Result<RealtimeWeather>> by lazy {
        val mutableLiveData = MutableLiveData<Result<RealtimeWeather>>()
        fetchWeather()
        mutableLiveData
    }

    val hangZhouRealtime:LiveData<Result<RealtimeWeather>>
    get() = _hangZhouRealtime


    fun fetchWeather() {
        viewModelScope.launch {
            _hangZhouRealtime.value=Result.Loading
            try {
                val hangzhouWeather: RealtimeWeather = repository.getHangzhouWeather()
                _hangZhouRealtime.value=Result.Success<RealtimeWeather>(hangzhouWeather)
            }catch (e:Exception){
                _hangZhouRealtime.value=Result.Error(e)
            }finally {
            }
        }
    }

}