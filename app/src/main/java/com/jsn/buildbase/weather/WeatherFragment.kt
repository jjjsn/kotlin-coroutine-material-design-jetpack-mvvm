package com.jsn.buildbase.weather

import android.os.Build
import android.view.View
import android.view.WindowInsets
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.jsn.baselibx.ui.BaseFragment
import com.jsn.baselibx.utils.JumpUtil
import com.jsn.buildbase.R
import com.jsn.buildbase.data.dataclass.Result
import com.jsn.buildbase.databinding.FragmentWeatherBinding
import com.jsn.buildbase.utilities.InjectorUtil
import com.jsn.buildbase.camera.CameraActivity
import com.jsn.buildbase.weather.ui.Main3Activity
import kotlinx.android.synthetic.main.fragment_homehaha.*

class WeatherFragment :BaseFragment<FragmentWeatherBinding>() {

    private val viewModel: WeatherViewModel by viewModels {
        InjectorUtil.provideWeatherViewModelFactory()
    }

    override fun getContentView(): Int {
        return R.layout.fragment_weather
    }

    override fun initUI() {

        viewModel.hangZhouRealtime.observe(viewLifecycleOwner){result->
            mBinding.tv.text=result.toString()
            mBinding.pb.visibility=if(result is Result.Loading) View.VISIBLE else View.GONE
            mBinding.tv.visibility=if(result is Result.Loading) View.GONE else View.VISIBLE
        }
    }

    override fun initData() {
        mBinding.tv.setOnClickListener{
            viewModel.fetchWeather()
        }
        bt.setSafeListener{ //mobile
            //navToDetail()
            //JumpUtil.overlay(requireContext(),ScrollingActivity::class.java)
            //JumpUtil.overlay(requireContext(),FullscreenActivity::class.java)
            //JumpUtil.overlay(requireContext(),Main2Activity::class.java)
           //mpUtil.overlay(requireContext(), Main3Activity::class.java)
            JumpUtil.overlay(requireContext(),
                NestActivity::class.java)
            //bt.circularRevealed()
           /* val view = bt
            ViewAnimationUtils.createCircularReveal(
                view,
                (view.left + view.right) / 2,
                (view.top + view.bottom) / 2,
                0f,
                max(view.width, view.height).toFloat()).apply {
                duration = 500
                start()
            }*/
        }
    }

    fun navToDetail(){
        val actionToDetail = WeatherFragmentDirections.actionToDetail(1)
        findNavController().navigate(actionToDetail)
    }
}

/*适配全面屏*/
@RequiresApi(Build.VERSION_CODES.KITKAT_WATCH)
fun View.applyWindowInsets(action: (View,WindowInsets)->Unit){
    setOnApplyWindowInsetsListener{view,insets->
        action(view,insets)
        insets
    }

}

//prevent from double click
inline fun View.setSafeListener( crossinline performClick:(View)->Unit){
    var lastClick:Long=0 //kotlin闭包
    
    setOnClickListener{
        val currentTimeMillis = System.currentTimeMillis()
        val interval = currentTimeMillis - lastClick
        lastClick=currentTimeMillis

        if(interval<500L) return@setOnClickListener //using a tag when return from lambda
        performClick(it)
    }
}

const val MAP_SDK_KEY="8e006dd9a14f02cc7ebb680623ed9cfb"