package com.jsn.buildbase.weather.ui.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.amap.api.maps.AMap
import com.amap.api.maps.model.MyLocationStyle
import com.jsn.baselibx.utils.Logs

class MapWeatherViewModel (val aMap: AMap): ViewModel() {

    init {
        Logs.e("amapinit")
        aMap.apply {
            MyLocationStyle().apply {
                interval(2000)
                //myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE)
                myLocationType(MyLocationStyle.LOCATION_TYPE_MAP_ROTATE)
                showMyLocation(true)
            }.also {
                    myLocationStyle=it
            }
            //uiSettings.isMyLocationButtonEnabled=true
            isMyLocationEnabled=true
            setOnMyLocationChangeListener { locationListener }
        }
    }

     var longitude= MutableLiveData<Double> ().apply {  }
     var latitude=MutableLiveData<Double>().apply {  }

    val locationListener=AMap.OnMyLocationChangeListener {
         latitude.value=it.latitude
         longitude .value= it.longitude
    }




}
