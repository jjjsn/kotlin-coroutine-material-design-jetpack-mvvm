package com.jsn.buildbase.weather.ui.map

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.amap.api.maps.AMap
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.jsn.baselibx.utils.Logs
import com.jsn.buildbase.BR

import com.jsn.buildbase.R
import com.jsn.buildbase.databinding.FragmentMapWeatherBinding
import com.jsn.buildbase.utilities.InjectorUtil
import com.jsn.buildbase.weather.setSafeListener
import kotlinx.android.synthetic.main.fragment_map_weather.*

class MapWeatherFragment : Fragment() {

    companion object {
        fun newInstance() = MapWeatherFragment()
    }

    private val  viewModel: MapWeatherViewModel by  viewModels {
        InjectorUtil.provideMapWeatherViewModelFactory(aMap)
    }

    lateinit var aMap:AMap

    lateinit var binding:ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=FragmentMapWeatherBinding.inflate(inflater,container,false).apply {
            lifecycleOwner=viewLifecycleOwner
        }
        return  binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        map_view.onCreate(savedInstanceState)

        aMap=map_view.map
        Logs.e("amapok")

        binding.setVariable(BR.viewModel,viewModel)

        bottom_sheet.setOnTouchListener{view,_->
            view.clearFocus()
            true
        }
        iv_cancel.setSafeListener {
            bottomCard(display = false)
        }

    }

    private fun bottomCard(display: Boolean) {
        val bottom = BottomSheetBehavior.from(bottom_sheet)
        bottom.state = if (display) BottomSheetBehavior.STATE_EXPANDED else BottomSheetBehavior.STATE_COLLAPSED
    }

    override fun onDestroy() {
        super.onDestroy()
        map_view ?.apply { onDestroy() }
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        map_view.onSaveInstanceState(outState)
    }

}

@BindingAdapter("doubleNumber")
fun bindLong(view: View,value:Double){
    (view as? TextView) ?. text=value.toString()
}

@Suppress("UNCHECKED_CAST")
class MapWeatherViewModelFactory(val aMap: AMap):ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MapWeatherViewModel(aMap) as T
    }
}
