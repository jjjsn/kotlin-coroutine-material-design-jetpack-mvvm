package com.jsn.buildbase.weather

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.lang.Exception
import kotlin.Lazy as Lazy1

class DetailViewModel(val weatherRepository: WeatherRepository,val value:Int) : ViewModel() {


    val listData:MutableLiveData<List<City>> by lazy{
        MutableLiveData<List<City>>().apply {
            value=cities
        }
    }

    val provinces by lazy {
        List<Province>(3){index->
            when(index){
                0-> Province("zhejiang", List<String>(3,{"aa";"cc";"ddd"}))
                1-> Province("beijing", List<String>(3,{"aa";"cc";"ddd"}))
                2-> Province("shanghai", List<String>(3,{"aa";"cc";"ddd"}))
               else-> throw Exception("error")
            }
        }
    }


    val cities :List<City> by lazy{
        mutableListOf<City>().apply {
            add(City("台州","zhejiang"))
            add(City("22","zhejiang"))
            add(City("33","zhejiang"))
            add(City("113311","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))
            add(City("1114441","zhejiang"))

            add(City("北京","beijing"))
            add(City("22","beijing"))
            add(City("33","beijing"))
            add(City("113311","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))
            add(City("1114441","beijing"))


            add(City("上海","shanghai"))
            add(City("22","shanghai"))
            add(City("33","shanghai"))
            add(City("113311","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
            add(City("1114441","shanghai"))
        }
    }
}


data class Province(val name:String,
                    val cities:List<String>){

}

data class City(val name:String,
                    val province:String){

}
