package com.jsn.buildbase.weather.ui.map

val mock:String="{\"status\":\"ok\",\"api_version\":\"v2.2\",\"api_status\":\"active\"," +
        "\"server_time\":1582964037,\"location\":[30.16,120.12],\"tzshift\":28800,\"lang\":\"zh_CN\"," +
        "\"unit\":\"metric\",\"result\":{\"status\":\"ok\"," +
        "\"temperature\":11.0,\"humidity\":0.86,\"cloudrate\":0.3,\"skycon\":\"PARTLY_CLOUDY_DAY\",\"visibility\":9.4,\"dswrf\":30.0," +
        "\"wind\":{\"speed\":4.68,\"direction\":78.0},\"pres\":100741.89,\"apparent_temperature\":10.1," +
        "\"precipitation\":{\"local\":{\"status\":\"ok\",\"datasource\":\"radar\",\"intensity\":0.0}," +
        "\"nearest\":{\"status\":\"ok\",\"distance\":11.18,\"intensity\":0.1875}},\"aqi\":19,\"pm25\":7," +
        "\"pm10\":13.0,\"o3\":20.0,\"so2\":4.0,\"no2\":38.0,\"co\":0.8,\"ultraviolet\":{\"index\":2.0," +
        "\"desc\":\"\\u5f88\\u5f31\"},\"comfort\":{\"index\":8,\"desc\":\"\\u5f88\\u51b7\"}}}"