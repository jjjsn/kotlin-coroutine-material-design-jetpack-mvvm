package com.jsn.buildbase.weather

import com.jsn.baselibx.retrofit.RetrofitApiFactory
import com.jsn.buildbase.data.WeatherApi
import com.jsn.buildbase.data.dataclass.RealtimeWeather
import java.util.concurrent.atomic.AtomicReference

class WeatherRepository private constructor(){

    val service=RetrofitApiFactory.createApi(WeatherApi::class.java)

    companion object{

        @Volatile private var instance: WeatherRepository?=null

        fun getInstance(): WeatherRepository =
            instance ?: synchronized(this){
                instance ?: WeatherRepository().also { instance =it }
            }


        private val singleInstance=AtomicReference<Any?>(null)

        fun getSingleInstance():WeatherRepository{

            while (true){
                val get = singleInstance.get() as WeatherRepository?
                if(get!=null) return get

                val newInstance=WeatherRepository()

                if(singleInstance.compareAndSet(null,newInstance)){
                    return newInstance
                }
            }
        }
    }

    suspend fun getHangzhouWeather(): RealtimeWeather =service.getHangzhouWeatherNow()

}