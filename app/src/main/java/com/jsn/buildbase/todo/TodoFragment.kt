package com.jsn.buildbase.todo

import com.jsn.baselibx.ui.BaseFragment
import com.jsn.buildbase.R
import com.jsn.buildbase.databinding.FragmentTodoBinding
import com.jsn.buildbase.databinding.FragmentWeatherBinding

class TodoFragment : BaseFragment<FragmentTodoBinding>() {
    override fun getContentView(): Int {
        return R.layout.fragment_todo
    }

    override fun initUI() {

    }

    override fun initData() {

    }

}