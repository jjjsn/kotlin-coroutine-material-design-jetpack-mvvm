package com.jsn.baselibx.util

import android.content.Context
import android.widget.Toast

fun Context.showMessage(m:String){
    Toast.makeText(this,m,Toast.LENGTH_SHORT).show()
}

fun Context.showMessageShort(s:String){
    Toast.makeText(applicationContext,s,Toast.LENGTH_SHORT)
        .show()}