package com.jsn.baselibx.retrofit;

import android.text.TextUtils;

import com.jsn.baselibx.utils.Logs;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * ：
 */
public class PrintIntercepter implements Interceptor {

    private static final String TAG = "AccessTokenInterceptor";

    /**
     *
     * @param chain
     * @return
     * @throws IOException
     */
    @Override
    public  /*synchronyed*/Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        Request.Builder requestBuilder = request.newBuilder();

        request = requestBuilder.header("Content-Type", "application/json;charset=UTF-8")
                .addHeader("x-rapidapi-host", "bestapi-ted-v1.p.rapidapi.com")  //ted header
                .addHeader("x-rapidapi-key", "e99e91bc2fmsh6a1f6e1d03ab5b4p1bdd7bjsn2e3295f8b956")
                .build();
        Response response = chain.proceed(request);

        /*tongbushuxin token
        * */
        Logs.e(String.format("发送请求 %s on %s%n%s",
                request.url(), request.headers(), bodyToString(request.body())));

        Logs.e(String.format("接收响应: [%s] %n返回json:【%s】 %s",
                response.request().url(),
                response.peekBody(1024 * 1024).string(),
                response.headers()));
        return response;
    }

    /**
     * 将请求提转为String
     *
     * @param request
     * @return
     */
    private static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "";
        }
    }

    /**
     * 将表单提交的内容转为JSON查看
     *
     * @param post
     * @return
     */
    public static String postStringToJson(String post) {
        String subJson[] = post.split("&");
        String newJson = "{";
        for (int i = 0; i < subJson.length; i++) {
            newJson += "\"" + subJson[i].replace("=", "\":\"") + "\",";
        }
        return newJson.substring(0, newJson.length() - 1) + "}";
    }


    /**
     * 请求是否可以注入参数
     *
     * @param request
     * @return
     */
    private boolean canInjectIntoBody(Request request) {

        if (request == null) {
            return false;
        }

        if (!TextUtils.equals(request.method(), "POST")) {
            return false;
        }

        RequestBody body = request.body();
        if (body == null) {
            return false;
        }

        MediaType mediaType = body.contentType();
        if (mediaType == null) {
            return false;
        }

        //针对 POST 表单 x-www-form-urlencoded 和 POST 实体 json
        if (!TextUtils.equals(mediaType.subtype(), "x-www-form-urlencoded") && !TextUtils.equals(mediaType.subtype(), "json")) {
            return false;
        }
        return true;
    }

}
