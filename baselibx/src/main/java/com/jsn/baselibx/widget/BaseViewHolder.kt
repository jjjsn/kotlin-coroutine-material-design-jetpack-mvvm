package com.jsn.baselibx.widget

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.jsn.baselibx.BR


abstract class BaseViewHolder(val binding:ViewDataBinding)
    :RecyclerView.ViewHolder(binding.root) {

    var _position :Int = 0

    open fun bindData(item: Any?){
        binding.setVariable(BR.item,item)
        binding.setVariable(BR.doClick,View.OnClickListener {
            doClick(it)
        })
        binding.executePendingBindings()
    }
    open fun doClick(view: View){

    }
}