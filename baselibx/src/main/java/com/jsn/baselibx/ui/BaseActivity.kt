package com.jsn.baselibx.ui

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.jsn.baselibx.utils.AppManager

 abstract class BaseActivity<T :ViewDataBinding> :AppCompatActivity()  {

    lateinit var TAG :String

    lateinit var mBinding:T

    lateinit var instance:BaseActivity<T>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance=this
        AppManager.getAppManager().addActivity(this)
        mBinding = DataBindingUtil.setContentView(this, getContentView())
        initContent()
    }
    fun initContent() {
        initUI()
        initData()
        initTAG(this)
    }

    /**
     * 日志TAG初始化
     *
     * @param context 上下文对象
     */
    fun initTAG(context: Context) {
        TAG = context.javaClass.getSimpleName()
    }

    /*获取布局*/
     abstract fun getContentView(): Int

    /**
     * 界面初始化
     */
     abstract fun initUI()

    /**
     * 数据初始化
     */
     abstract fun initData()

    fun getContext(): Context {
        return instance
    }

    fun getActivity(): BaseActivity<*> {
        return instance
    }

    fun showProgress(message: String) {

    }

    fun hideProgress() {

    }

    fun showMessage(message: String) {
        hideProgress()

    }
}