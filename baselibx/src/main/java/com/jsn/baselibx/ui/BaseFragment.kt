package com.jsn.baselibx.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

 abstract class BaseFragment<T :ViewDataBinding> :Fragment(){

    lateinit var TAG :String

    lateinit var mBinding:T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTAG(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initUI()
        initData()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)

        mBinding=DataBindingUtil.inflate(inflater,getContentView(),container,false)

        return mBinding.root
    }

    protected fun initTAG(fragment: Fragment) {
        TAG = fragment.javaClass.simpleName ?:""
    }

    /**
     * 获取布局
     */
     abstract fun getContentView(): Int


    /**
     * 界面初始化
     */
     abstract fun initUI()

    /**
     * 数据初始化
     */
     abstract fun initData()

    fun showProgress(message: String) {
        //MLoadingDialog.show(activity, message)
    }

    fun hideProgress() {
        //MLoadingDialog.dismiss()
    }

    fun showMessage(msg:String){

    }
}