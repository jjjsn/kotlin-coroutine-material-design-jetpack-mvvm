package com.jsn.baselibx

import android.app.Application
import android.content.Context
import android.widget.Toast
import androidx.core.app.NavUtils
import com.jsn.baselibx.utils.AppManager


open class MApp : Application() {

    companion object{
       @JvmStatic var instance:MApp?=null
    }

    lateinit var appManager:AppManager

    init {
        instance=this
    }

    override fun onCreate() {
        super.onCreate()
        appManager = AppManager.getAppManager()
        appManager.init(this)
    }

}