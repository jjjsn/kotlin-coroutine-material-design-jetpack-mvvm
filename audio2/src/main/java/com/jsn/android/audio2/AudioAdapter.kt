package com.jsn.android.audio2

import android.renderscript.ScriptGroup
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jsn.android.audio2.databinding.ItemAudioBinding

val callBack=object:DiffUtil.ItemCallback<AudioTrackActivity.Audio>(){
    override fun areItemsTheSame(
        oldItem: AudioTrackActivity.Audio,
        newItem: AudioTrackActivity.Audio
    ): Boolean {
        return false
    }

    override fun areContentsTheSame(
        oldItem: AudioTrackActivity.Audio,
        newItem: AudioTrackActivity.Audio
    ): Boolean {
        return false
    }

}

class AudioAdapter :ListAdapter<AudioTrackActivity.Audio,AudioViewHolder>(callBack){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AudioViewHolder {
        val inflate: ItemAudioBinding = ItemAudioBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AudioViewHolder(inflate)
    }

    override fun onBindViewHolder(holder: AudioViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class AudioViewHolder(val binding :ItemAudioBinding):RecyclerView.ViewHolder(binding.root){
    fun bind(item:AudioTrackActivity.Audio){
        binding.audio=item
        binding.executePendingBindings()
    }
}
