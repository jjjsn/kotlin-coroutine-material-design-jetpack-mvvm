package com.jsn.android.audio2.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Mp4(
    @PrimaryKey val vid:Int,
    @ColumnInfo(name ="name") val name:String?,
    @ColumnInfo(name ="lastModified") val time:String?
)