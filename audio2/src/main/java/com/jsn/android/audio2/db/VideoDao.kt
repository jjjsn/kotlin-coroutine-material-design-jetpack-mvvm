package com.jsn.android.audio2.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import java.nio.file.attribute.UserDefinedFileAttributeView

@Dao
interface VideoDao {
    @Query("SELECT * FROM mp4")
    fun getAll():LiveData<List<Mp4>>

    @Insert
    fun insert(vararg  mp4s:Mp4)


    @Delete
    fun delete(mp4: Mp4)
}