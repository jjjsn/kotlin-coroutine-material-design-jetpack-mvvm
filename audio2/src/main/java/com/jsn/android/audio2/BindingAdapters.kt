package com.jsn.android.audio2

import android.widget.Button
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("audios")
fun bindAudio(view: RecyclerView, audios:Result<List<AudioTrackActivity.Audio>>){
    val audioAdapter = view.adapter as? AudioAdapter
    audioAdapter?.submitList(audios.successOr(emptyList()))
}
@BindingAdapter("loading")
fun loading(view: ProgressBar, audios:Result<List<AudioTrackActivity.Audio>>){
    view.visible(audios is Result.Loading)
}
@BindingAdapter("btok")
fun btEnbaled(view: Button, audios:Result<List<AudioTrackActivity.Audio>>){
    view.isEnabled=!(audios is Result.Loading)
}