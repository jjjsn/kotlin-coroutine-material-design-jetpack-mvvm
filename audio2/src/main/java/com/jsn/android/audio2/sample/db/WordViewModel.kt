package com.jsn.android.audio2.sample.db

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class WordViewModel(application: Application) :AndroidViewModel(application){
    val repository:WordRepository

    val allWords :LiveData<List<Word>>
    init {
        val wordDao = WordRoomDataBase.getInstance(application,viewModelScope).wordDao()
        repository=WordRepository(wordDao)
        allWords=repository.allWords
    }

    fun insert(word: Word)=viewModelScope.launch {
       repository.insert(word) //suspend
    }
}