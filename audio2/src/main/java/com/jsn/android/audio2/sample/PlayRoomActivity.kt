package com.jsn.android.audio2.sample

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.jsn.android.audio2.R
import com.jsn.android.audio2.ViewModel
import com.jsn.android.audio2.databinding.ActivityRoomBinding
import com.jsn.android.audio2.sample.db.Word
import com.jsn.android.audio2.sample.db.WordViewModel
import com.jsn.baselibx.ui.BaseActivity

class RoomActivity:BaseActivity<ActivityRoomBinding>(){
    override fun getContentView(): Int {
        return R.layout.activity_room
    }

    lateinit var wordViewModel: WordViewModel

    override fun initUI() {
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = WordListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        wordViewModel=ViewModelProvider(this,WordViewModelFactory(this)).get(WordViewModel::class.java)

        wordViewModel.allWords.observe(this){words->
            adapter.setWords(words)
        }
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this, NewWordActivity::class.java)
            startActivityForResult(intent, newWordActivityRequestCode)
        }
    }

    private val newWordActivityRequestCode = 1

    override fun initData() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newWordActivityRequestCode && resultCode == Activity.RESULT_OK) {
            data?.getStringExtra(NewWordActivity.EXTRA_REPLY)?.let {
                val word = Word(it)
                wordViewModel.insert(word)
            }
        } else {
            Toast.makeText(
                applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG).show()
        }
    }

}

@Suppress("UNCHECKED_CAST")
class WordViewModelFactory(val context: Context):ViewModelProvider.NewInstanceFactory(){
    override fun <T : androidx.lifecycle.ViewModel?> create(modelClass: Class<T>): T {
        return WordViewModel(context.applicationContext as Application) as T
    }
}

class WordListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<WordListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var words = emptyList<Word>() // Cached copy of words

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val wordItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = words[position]
        holder.wordItemView.text = current.word
    }

    internal fun setWords(words: List<Word>) {
        this.words = words
        notifyDataSetChanged()
    }

    override fun getItemCount() = words.size
}